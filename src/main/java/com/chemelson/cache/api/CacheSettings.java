package com.chemelson.cache.api;

import com.chemelson.cache.impl.CacheType;

public interface CacheSettings {

    int capacity();

    CacheType type();

    interface Builder {

        CacheSettings.Builder capacity(int capacity);

        CacheSettings.Builder type(CacheType type);

        CacheSettings build();
    }
}
