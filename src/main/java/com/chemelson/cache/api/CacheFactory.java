package com.chemelson.cache.api;

import com.chemelson.cache.impl.CacheType;
import com.chemelson.cache.impl.LFUCache;
import com.chemelson.cache.impl.LRUCache;

public final class CacheFactory<K, V> {

    public Cache<K, V> create(CacheSettings settings) {
        int capacity = settings.capacity();
        CacheType type = settings.type();
        switch (type) {
            case LRU:
                return new LRUCache<>(capacity);
            case LFU:
                return new LFUCache<>(capacity);
            default:
                throw new IllegalArgumentException("Unknown cache type provided: " + type.name());
        }
    }

}
