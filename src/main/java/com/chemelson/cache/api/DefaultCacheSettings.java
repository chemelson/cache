package com.chemelson.cache.api;

import com.chemelson.cache.impl.CacheType;

public final class DefaultCacheSettings implements CacheSettings {

    private final int capacity;
    private final CacheType type;

    private DefaultCacheSettings(int capacity, CacheType type) {
        this.capacity = capacity;
        this.type = type;
    }

    @Override
    public int capacity() {
        return capacity;
    }

    @Override
    public CacheType type() {
        return type;
    }

    public static Builder builder() {
        return new DefaultCacheSettings.BuilderImpl();
    }

    public static final class BuilderImpl implements CacheSettings.Builder {

        private int capacity;
        private CacheType type;

        private BuilderImpl() {
        }

        @Override
        public CacheSettings.Builder capacity(int capacity) {
            if (capacity <= 0) {
                throw new IllegalArgumentException("Cache size must be positive non zero value");
            }
            this.capacity = capacity;
            return this;
        }

        @Override
        public CacheSettings.Builder type(CacheType type) {
            this.type = type;
            return this;
        }

        @Override
        public CacheSettings build() {
            return new DefaultCacheSettings(this.capacity, this.type);
        }
    }
}
