package com.chemelson.cache.api;

import java.util.Collection;
import java.util.Optional;

public interface Cache<K, V> {

    Collection<K> getKeys();

    boolean containsKey(K key);

    Optional<V> get(K key);

    void put(K key, V value);

    void remove(K key);

    void clear();

}
