package com.chemelson.cache.impl;

import java.util.Optional;

public final class LFUCache<K, V> extends AbstractCache<K, V> {

    public LFUCache(int capacity) {
        super(capacity);
    }

    @Override
    public Optional<V> get(K key) {
        if (keyToEntry.containsKey(key)) {
            CacheEntry<K, V> entry = keyToEntry.get(key);
            removeEntry(entry);
            entry.incrementFrequency();
            promoteEntry(entry);
            return Optional.ofNullable(entry.value());
        }
        return Optional.empty();
    }

    @Override
    public void put(K key, V value) {
        if (keyToEntry.containsKey(key)) {
            CacheEntry<K, V> entry = keyToEntry.get(key);
            entry.value(value);
            entry.incrementFrequency();
            removeEntry(entry);
            promoteEntry(entry);
        } else {
            CacheEntry<K, V> newEntry = new CacheEntry<>(key, value, 1, null, null);
            if (keyToEntry.size() == capacity) {
                keyToEntry.remove(start.key());
                removeEntry(start);
            }
            promoteEntry(newEntry);
            keyToEntry.put(key, newEntry);
        }
    }

    @Override
    protected void promoteEntry(CacheEntry<K, V> entry) {
        if (start != null && end != null) {
            CacheEntry<K, V> currentEntry = start;
            while (true) {
                if (currentEntry.frequency() > entry.frequency()) {
                    if (currentEntry == start) {
                        entry.right(currentEntry);
                        currentEntry.left(entry);
                        start = entry;
                        break;
                    } else {
                        entry.right(currentEntry);
                        entry.left(currentEntry.left());
                        currentEntry.left().right(entry);
                        entry.left(currentEntry.left());
                        break;
                    }
                } else {
                    currentEntry = currentEntry.right();
                    if (currentEntry == null) {
                        end.right(entry);
                        entry.left(end);
                        entry.right(null);
                        end = entry;
                        break;
                    }
                }
            }
        } else {
            end = entry;
            start = end;
        }
    }
}
