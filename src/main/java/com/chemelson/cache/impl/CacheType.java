package com.chemelson.cache.impl;

public enum CacheType {
    LRU, LFU
}
