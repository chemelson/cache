package com.chemelson.cache.impl;

import java.util.Optional;

public final class LRUCache<K, V> extends AbstractCache<K, V> {

    public LRUCache(int capacity) {
        super(capacity);
    }

    @Override
    public Optional<V> get(K key) {
        if (keyToEntry.containsKey(key)) {
            CacheEntry<K, V> entry = keyToEntry.get(key);
            removeEntry(entry);
            promoteEntry(entry);
            return Optional.ofNullable(entry.value());
        }
        return Optional.empty();
    }

    @Override
    public void put(K key, V value) {
        if (keyToEntry.containsKey(key)) {
            CacheEntry<K, V> entry = keyToEntry.get(key);
            entry.value(value);
            removeEntry(entry);
            promoteEntry(entry);
        } else {
            CacheEntry<K, V> newEntry = new CacheEntry<>(key, value, null, null);
            if (keyToEntry.size() == capacity) {
                keyToEntry.remove(end.key());
                removeEntry(end);
            }
            promoteEntry(newEntry);
            keyToEntry.put(key, newEntry);
        }
    }

    @Override
    public void promoteEntry(CacheEntry<K, V> entry) {
        entry.right(start);
        entry.left(null);

        if (start != null) {
            start.left(entry);
        }
        start = entry;

        if (end == null) {
            end = start;
        }
    }

}

