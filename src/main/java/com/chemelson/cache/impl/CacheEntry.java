package com.chemelson.cache.impl;

public final class CacheEntry<K, V> {

    private final K key;
    private V value;
    private Integer frequency;
    private CacheEntry<K, V> left;
    private CacheEntry<K, V> right;

    public CacheEntry(K key, V value, Integer frequency, CacheEntry<K, V> left, CacheEntry<K, V> right) {
        this.key = key;
        this.value = value;
        this.frequency = frequency;
        this.left = left;
        this.right = right;
    }

    public CacheEntry(K key, V value, CacheEntry<K, V> left, CacheEntry<K, V> right) {
        this(key, value, null, left, right);
    }

    public K key() {
        return key;
    }

    public V value() {
        return value;
    }

    public void value(V value) {
        this.value = value;
    }

    public Integer frequency() {
        return frequency;
    }

    public void incrementFrequency() {
        if (frequency == null) {
            throw new IllegalStateException("Entry is not properly used");
        } else {
            frequency = frequency + 1;
        }
    }

    public CacheEntry<K, V> left() {
        return left;
    }

    public void left(CacheEntry<K, V> left) {
        this.left = left;
    }

    public CacheEntry<K, V> right() {
        return right;
    }

    public void right(CacheEntry<K, V> right) {
        this.right = right;
    }
}
