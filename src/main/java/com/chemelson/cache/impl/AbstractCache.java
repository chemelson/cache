package com.chemelson.cache.impl;

import com.chemelson.cache.api.Cache;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public abstract class AbstractCache<K, V> implements Cache<K, V> {

    protected final Map<K, CacheEntry<K, V>> keyToEntry;
    protected CacheEntry<K, V> start, end;
    protected final int capacity;

    protected AbstractCache(int capacity) {
        this.capacity = capacity;
        this.keyToEntry = new HashMap<>();
    }

    public abstract Optional<V> get(K key);

    public abstract void put(K key, V value);

    protected abstract void promoteEntry(CacheEntry<K, V> entry);

    public Collection<K> getKeys() {
        return keyToEntry.keySet();
    }

    public boolean containsKey(K key) {
        return keyToEntry.containsKey(key);
    }

    public void remove(K key) {
        if (keyToEntry.containsKey(key)) {
            removeEntry(keyToEntry.get(key));
            keyToEntry.remove(key);
        }
    }

    public void clear() {
        keyToEntry.clear();
        start = null;
        end = null;
    }

    protected void removeEntry(CacheEntry<K, V> entry) {
        if (entry.left() != null) {
            entry.left().right(entry.right());
        } else {
            start = entry.right();
        }

        if (entry.right() != null) {
            entry.right().left(entry.left());
        } else {
            end = entry.left();
        }
    }
}
