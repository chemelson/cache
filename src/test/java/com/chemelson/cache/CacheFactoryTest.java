package com.chemelson.cache;

import com.chemelson.cache.api.CacheFactory;
import com.chemelson.cache.api.CacheSettings;
import com.chemelson.cache.api.DefaultCacheSettings;
import com.chemelson.cache.impl.CacheType;
import com.chemelson.cache.impl.LFUCache;
import com.chemelson.cache.impl.LRUCache;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertSame;

public final class CacheFactoryTest {

    private final CacheFactory<String, String> factory = new CacheFactory<>();

    @Test
    public void createLRUCache() {
        CacheSettings lruCacheSettings = buildSettingsOfType(CacheType.LRU);
        assertSame(LRUCache.class, factory.create(lruCacheSettings).getClass());
    }

    @Test
    public void createLFUCache() {
        CacheSettings lfuCacheSettings = buildSettingsOfType(CacheType.LFU);
        assertSame(LFUCache.class, factory.create(lfuCacheSettings).getClass());
    }

    private CacheSettings buildSettingsOfType(CacheType type) {
        return DefaultCacheSettings.builder()
                .capacity(10)
                .type(type)
                .build();
    }
}
