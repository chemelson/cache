package com.chemelson.cache;

import com.chemelson.cache.impl.CacheType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.chemelson.cache.CacheTestConstants.*;
import static org.junit.jupiter.api.Assertions.*;

public final class LFUCacheTest extends BaseCacheTest {

    @BeforeEach
    public void setUp() {
        initCacheWithType(CacheType.LFU);
    }

    @Test
    public void putNewValueInFullCache() {
        // Make cache contain 3 entries with frequency 1
        cache.put(SECOND_KEY, SECOND_VALUE);
        cache.put(THIRD_KEY, THIRD_VALUE);

        // Make second key value the candidate for eviction
        cache.get(FIRST_KEY);
        cache.get(FIRST_KEY);
        cache.get(THIRD_KEY);

        cache.put(FOURTH_KEY, FOURTH_VALUE);

        assertTrue(cache.containsKey(FOURTH_KEY));
        assertFalse(cache.containsKey(SECOND_KEY));
        assertEquals(3, cache.getKeys().size());
    }
}
