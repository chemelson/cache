package com.chemelson.cache;

import com.chemelson.cache.api.Cache;
import com.chemelson.cache.api.CacheFactory;
import com.chemelson.cache.api.CacheSettings;
import com.chemelson.cache.api.DefaultCacheSettings;
import com.chemelson.cache.impl.CacheType;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;
import java.util.Set;

import static com.chemelson.cache.CacheTestConstants.*;
import static org.junit.jupiter.api.Assertions.*;

public abstract class BaseCacheTest {

    CacheFactory<String, String> factory = new CacheFactory<>();
    Cache<String, String> cache;

    protected void initCacheWithType(CacheType type) {
        CacheSettings cacheSettings = DefaultCacheSettings.builder()
                .capacity(3)
                .type(type)
                .build();
        cache = factory.create(cacheSettings);
        cache.put(FIRST_KEY, FIRST_VALUE);
    }

    @Test
    public void getExistingKeyValue() {
        assertEquals(FIRST_VALUE, cache.get(FIRST_KEY).orElseThrow());
    }

    @Test
    public void getNonExistingKeyValue() {
        assertThrows(NoSuchElementException.class, () -> cache.get(UNKNOWN_KEY).orElseThrow());
    }

    @Test
    public void updateExistingValue() {
        cache.put(FIRST_KEY, NEW_FIRST_VALUE);

        assertEquals(NEW_FIRST_VALUE, cache.get(FIRST_KEY).orElseThrow());
        assertEquals(1, cache.getKeys().size());
    }

    @Test
    public void putNewValue() {
        cache.put(SECOND_KEY, SECOND_VALUE);

        assertTrue(cache.containsKey(SECOND_KEY));
        assertEquals(2, cache.getKeys().size());
    }

    @Test
    public void removeExistingKeyValue() {
        cache.remove(FIRST_KEY);

        assertFalse(cache.containsKey(FIRST_KEY));
        assertEquals(0, cache.getKeys().size());
    }

    @Test
    public void getCacheKeys() {
        cache.put(SECOND_KEY, SECOND_VALUE);
        cache.put(THIRD_KEY, THIRD_VALUE);

        Set<String> keySet = Set.of(FIRST_KEY, SECOND_KEY, THIRD_KEY);

        assertEquals(keySet, cache.getKeys());
    }

    @Test
    public void clearCache() {
        cache.clear();

        assertEquals(0, cache.getKeys().size());
    }
}
