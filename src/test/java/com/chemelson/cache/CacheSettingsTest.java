package com.chemelson.cache;

import com.chemelson.cache.api.DefaultCacheSettings;
import com.chemelson.cache.impl.CacheType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public final class CacheSettingsTest {

    @Test
    public void createSettingsWithWrongSize() {
        assertThrows(IllegalArgumentException.class, () -> DefaultCacheSettings.builder()
                .capacity(0)
                .type(CacheType.LRU)
                .build());
    }

}
