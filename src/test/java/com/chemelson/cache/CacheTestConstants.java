package com.chemelson.cache;

public final class CacheTestConstants {
    public static final String UNKNOWN_KEY = "unknownKey";

    public static final String FIRST_KEY = "firstKey";
    public static final String FIRST_VALUE = "firstValue";
    public static final String NEW_FIRST_VALUE = "newFirstValue";

    public static final String SECOND_KEY = "secondKey";
    public static final String SECOND_VALUE = "secondValue";

    public static final String THIRD_KEY = "thirdKey";
    public static final String THIRD_VALUE = "thirdValue";

    public static final String FOURTH_KEY = "fourthKey";
    public static final String FOURTH_VALUE = "fourthValue";
}
