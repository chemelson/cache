package com.chemelson.cache;

import com.chemelson.cache.impl.CacheType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.chemelson.cache.CacheTestConstants.*;
import static org.junit.jupiter.api.Assertions.*;

public final class LRUCacheTest extends BaseCacheTest {

    @BeforeEach
    public void setUp() {
        initCacheWithType(CacheType.LRU);
    }

    @Test
    public void putNewValueInFullCache() {
        cache.put(SECOND_KEY, SECOND_VALUE);
        cache.put(THIRD_KEY, THIRD_VALUE);

        cache.put(FOURTH_KEY, FOURTH_VALUE);

        assertTrue(cache.containsKey(FOURTH_KEY));
        assertFalse(cache.containsKey(FIRST_KEY));
        assertEquals(3, cache.getKeys().size());
    }

}
